# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.db.models.signals import post_save, post_delete


class GccfarmUserConfig(AppConfig):
    name = 'gccfarm_user'

    def ready(self):
        from .signals import add_user_to_sshkey_deploy_queue
        post_save.connect(add_user_to_sshkey_deploy_queue, sender='simplesshkey.UserKey',
                          dispatch_uid='queue_when_save')
        post_delete.connect(add_user_to_sshkey_deploy_queue, sender='simplesshkey.UserKey',
                          dispatch_uid='queue_when_delete')
