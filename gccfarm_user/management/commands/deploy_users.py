# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

from ansible_farm import deploy


class Command(BaseCommand):
    help = 'Create specified Unix users on farm machines.  Runs in dry-run mode by default.'

    def add_arguments(self, parser):
        parser.add_argument('user', nargs='*',
                            help='Only create accounts for the specified user(s) (default: all users)')
        parser.add_argument('--machine', '-m', action='append',
                            help='Only create accounts on the specified machine(s) (default: all machines)')
        parser.add_argument('--apply', action='store_true',
                            help="Actually apply the changes")

    def handle(self, *args, **options):
        deploy.deploy_users(options['user'], options['machine'], not options['apply'])
