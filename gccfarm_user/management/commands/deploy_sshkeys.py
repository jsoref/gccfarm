# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

import filelock

from ansible_farm import deploy


class Command(BaseCommand):
    help = 'Deploy SSH keys for the specified users on farm machines.  Runs in dry-run mode by default.'

    def add_arguments(self, parser):
        parser.add_argument('user', nargs='*',
                            help='Only deploy keys for the specified user(s) (default: all users)')
        parser.add_argument('--machine', '-m', action='append',
                            help='Only deploy keys on the specified machine(s) (default: all machines)')
        parser.add_argument('--apply', action='store_true',
                            help="Actually apply the changes")

    def handle(self, *args, **options):
        lock = filelock.FileLock('/tmp/gccfarm-deploy-sshkeys.lock')
        lock.timeout = 0
        try:
            with lock:
                deploy.deploy_sshkeys(options['user'], options['machine'], not options['apply'])
        except filelock.Timeout:
            self.stderr.write('Another SSH keys deployment is already running')
