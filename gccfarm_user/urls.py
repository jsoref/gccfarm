# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from gccfarm_user import views


urlpatterns = [
    url(r'^new/$', views.FarmUserCreateView.as_view(), name='join'),
    url(r'^review/all/$', views.AccountRequestList.as_view(), name='review_account_requests'),
    url(r'^review/all/approved/$', views.ApprovedUserList.as_view(), name='approved_accounts'),
    url(r'^review/all/rejected/$', views.RejectedUserList.as_view(), name='rejected_accounts'),
    url(r'^review/approve/(?P<pk>[0-9]+)/$', views.ApproveAccountRequest.as_view(), name='approve_account_request'),
    url(r'^review/reject/(?P<pk>[0-9]+)/$', views.RejectAccountRequest.as_view(), name='reject_account_request'),
    url(r'^dashboard/$', views.DeploymentDashboard.as_view(), name='deployment_dashboard'),
]
