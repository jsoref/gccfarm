# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import FarmUser, SSHKeyDeployQueue, SSHKeyDeployBacklog


farmuser_fieldsets = list(UserAdmin.fieldsets)
farmuser_fieldsets.insert(2, ('GCC Farm',
                              {'fields':
                               ('uid', 'contributions', 'deployment_started', 'request_date', 'approved', 'reject_reason', 'approved_by', 'approved_date')}))

class FarmUserAdmin(UserAdmin):
    list_display = ('username', 'uid', 'email', 'name', 'request_date', 'approved', 'approved_by', 'approved_date', 'is_active')
    list_filter = ('approved', 'deployment_started', 'is_active', 'is_superuser', 'groups')
    readonly_fields = ('deployment_started', 'request_date', 'approved', 'approved_by', 'approved_date', 'reject_reason')
    ordering = ('uid', 'username')
    fieldsets = farmuser_fieldsets

    def name(self, obj):
        return '{} {}'.format(obj.first_name, obj.last_name).strip()
    name.admin_order_field = 'first_name'


class SSHKeyDeployQueueAdmin(admin.ModelAdmin):
    list_display = ('user', 'added')


class SSHKeyDeployBacklogAdmin(admin.ModelAdmin):
    list_display = ('user', 'machine', 'added')


admin.site.register(FarmUser, FarmUserAdmin)
admin.site.register(SSHKeyDeployQueue, SSHKeyDeployQueueAdmin)
admin.site.register(SSHKeyDeployBacklog, SSHKeyDeployBacklogAdmin)
