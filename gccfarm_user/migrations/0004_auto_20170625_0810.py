# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-25 08:10
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import gccfarm_user.models


class Migration(migrations.Migration):

    dependencies = [
        ('gccfarm_user', '0003_auto_20170619_1945_squashed_0004_auto_20170619_2120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='farmuser',
            name='username',
            field=models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 30 characters or fewer.', max_length=30, unique=True, validators=[django.core.validators.RegexValidator('^[a-z][a-z0-9_-]*$', 'Enter a valid Unix login. It must start with a lowercase letter and may only contain lowercase letters, numbers, dash and underscore.'), gccfarm_user.models.validate_allowed_username], verbose_name='username'),
        ),
    ]
