from fabric.api import *
from fabvenv import virtualenv

env.hosts = [
        'cfarm.tetaneutral.net:2222'
]

env.user = "admin"

project_dir = '/var/www/html/gccfarm/'
env_dir = project_dir + 'venv/'

def deploy():
    with cd(project_dir):
        sudo("systemctl stop gccfarm.service")
        
        run("git pull")

        with virtualenv(env_dir):
            run("pip install -r requirements.txt")
            run("./manage.py collectstatic --noinput")
            run("./manage.py migrate")

        sudo("systemctl start gccfarm.service")
