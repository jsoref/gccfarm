from django.shortcuts import render


def home(request):
    return render(request, 'gccfarm_web/home.html')

def join_confirmation(request):
    return render(request, 'gccfarm_web/join_confirmation.html')

def tickets(request):
    return render(request, 'gccfarm_web/tickets.html')

def manage_machines(request):
    return render(request, 'gccfarm_web/manage_machines.html')

def manage_groups(request):
    return render(request, 'gccfarm_web/manage_groups.html')

def see_users(request):
    return render(request, 'gccfarm_web/see_users.html')
