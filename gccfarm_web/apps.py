from django.apps import AppConfig


class GccfarmWebConfig(AppConfig):
    name = 'gccfarm_web'
