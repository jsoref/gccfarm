"""gccfarm_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include

from gccfarm_web import views
import simplesshkey.views

sshkey_patterns = ([
    url(r'^create/', simplesshkey.views.userkey_add, name='userkey_add'),
    url(r'^list/', simplesshkey.views.userkey_list, name='userkey_list'),
    url(r'^(?P<pk>\d+)/delete$', simplesshkey.views.userkey_delete, name='userkey_delete'),
], 'simplesshkey')

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^tickets/', views.tickets, name='tickets'),
    url(r'^join/done/', views.join_confirmation, name='join_confirmation'),
    #url(r'^machines/', views.manage_machines, name='manage_machines'),
    #url(r'^groups/', views.manage_groups, name='manage_groups'),
    #url(r'^users/', views.see_users, name='see_users'),
    url(r'^sshkey/', include(sshkey_patterns)),
]
