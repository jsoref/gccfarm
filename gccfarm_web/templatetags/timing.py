# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template
from django.utils.safestring import mark_safe
from django.contrib.humanize.templatetags import humanize

register = template.Library()


@register.filter
def naturaltime_with_hover(datetime):
    """Same as naturaltime, but adds the absolute date and time as a hover"""
    res = '<span title="{}">{}</span>'.format(datetime.strftime("%d %B %Y, %H:%M"),
                                              humanize.naturaltime(datetime))
    return mark_safe(res)
