from django.core import mail
from django.template import loader


def send_templated_mail(context, subject_template_name, email_template_name,
                        from_email, recipient_list):
    subject = loader.render_to_string(subject_template_name, context)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context)
    mail.send_mail(subject, body, from_email, recipient_list)


def send_templated_mail_managers(context, subject_template_name, email_template_name,
                               fail_silently=False):
    subject = loader.render_to_string(subject_template_name, context)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context)
    mail.mail_managers(subject, body, fail_silently)
