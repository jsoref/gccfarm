# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import models


class Machine(models.Model):
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    # Manually-managed fields
    name = models.CharField(max_length=64, unique=True,
                            help_text='Short name of the machine')
    hostname = models.CharField(max_length=255,
                                help_text='Fully qualified hostname used to connect to the machine')
    description = models.CharField(max_length=255, blank=True,
                                   help_text='Short summary of the specificities or purpose of the machine')
    host = models.CharField(max_length=255, blank=True,
                            help_text='Organisation hosting the machine')
    location = models.CharField(max_length=255, blank=True,
                                help_text='Physical location (country, city) of the machine')
    largest_deployed_uid = models.IntegerField(default=0, verbose_name='largest deployed UID',
                                               help_text='Largest user UID deployed on this machine (without offset), used to determine which users still need be deployed.  Change this with caution!')
    uid_offset = models.IntegerField(default=0, verbose_name='UID offset',
                                     help_text='Offset applied to the UID of deployed users (e.g. UID=1042,offset=1000 => effective deployed UID is 2042')

    # Fields that are automatically filled by Ansible
    # Metadata
    success = models.BooleanField(default=False, editable=False,
                                  help_text='Whether the last gathering process was successful')
    last_success = models.DateTimeField(null=True, editable=False,
                                        help_text='Date of the last successful gathering process')
    # Actual technical data
    architecture = models.CharField(max_length=512, blank=True, editable=False)
    kernel = models.CharField(max_length=512, blank=True, editable=False)
    os = models.CharField(max_length=255, blank=True, editable=False)
    processor = models.CharField(max_length=512, blank=True, editable=False)
    nb_cpu = models.PositiveIntegerField(default=0, editable=False,
                                         help_text="Number of physical distinct CPUs")
    nb_cores = models.PositiveIntegerField(default=0, editable=False,
                                           help_text="Total number of CPU cores")
    nb_threads = models.PositiveIntegerField(default=0, editable=False,
                                             help_text="Total number of CPU threads")
    memtotal_mb = models.BigIntegerField(default=0, editable=False)
    swaptotal_mb = models.BigIntegerField(default=0, editable=False)
    mount_root_fstype = models.CharField(max_length=64, blank=True, editable=False)
    mount_root_size_total = models.BigIntegerField(default=0, editable=False,
                                                   help_text='In bytes')
    mount_root_size_available = models.BigIntegerField(default=0, editable=False,
                                                       help_text='In bytes')
    mount_home_fstype = models.CharField(max_length=64, blank=True, editable=False)
    mount_home_size_total = models.BigIntegerField(default=0, editable=False,
                                                   help_text='In bytes')
    mount_home_size_available = models.BigIntegerField(default=0, editable=False,
                                                       help_text='In bytes')
    uptime = models.DurationField(default=datetime.timedelta, editable=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
