# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from gccfarm_machine import views


urlpatterns = [
    url(r'^list/$', views.MachineList.as_view(), name='platform'),
]
