# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Machine


class MachineAdmin(admin.ModelAdmin):
    list_display = ['name', 'host', 'location', 'largest_deployed_uid', 'success', 'last_success']
    list_filter = ['host', 'location', 'success', 'largest_deployed_uid']
    readonly_fields = ['success', 'last_success']

admin.site.register(Machine, MachineAdmin)
