# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic.list import ListView

from gccfarm_machine.models import Machine


class MachineList(ListView):
    model = Machine
    template_name = 'gccfarm_machine/machines_list.html'
    context_object_name = 'machines'
