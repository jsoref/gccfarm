"""gccfarm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

from gccfarm_user import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^machines/', include('gccfarm_machine.urls')),
    url(r'^users/', include('gccfarm_user.urls')),
    # Override this specific view to allow resetting password for users without password
    url(r'^password_reset/$',
        auth_views.PasswordResetView.as_view(form_class=views.CustomPasswordResetForm),
        name='password_reset'),
    url('^', include('django.contrib.auth.urls')),
    url(r'^', include('gccfarm_web.urls')),
]
