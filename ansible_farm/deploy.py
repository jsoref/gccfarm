# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

import sys
from collections import defaultdict

from django.conf import settings
import ansible.plugins.callback.default

from gccfarm_user.models import FarmUser, SSHKeyDeployQueue, SSHKeyDeployBacklog
from gccfarm_machine.models import Machine

from boilerplate import run_play


def exclude_forbidden_users(user_queryset):
    """Given a FarmUser queryset, exclude any user that is forbidden from
    deployment (based on the settings CFARM_FORBIDDEN_USERNAMES and
    CFARM_FORBIDDEN_UID).

    This function does not evaluate the queryset passed as argument.
    """
    forbidden_users = user_queryset.filter(username__in=settings.CFARM_FORBIDDEN_USERNAMES)
    forbidden_uid = user_queryset.filter(uid__in=settings.CFARM_FORBIDDEN_UID)
    if len(forbidden_users) > 0:
        print("** WARNING: users {} are forbidden from deployment (wrong username) **".format(list(forbidden_users)))
    if len(forbidden_uid) > 0:
        print("** WARNING: users {} are forbidden from deployment (wrong UID) **".format(list(forbidden_uid)))
    return user_queryset.exclude(username__in=settings.CFARM_FORBIDDEN_USERNAMES).exclude(uid__in=settings.CFARM_FORBIDDEN_UID)


class UserDeployCallback(ansible.plugins.callback.default.CallbackModule):
    """This class is only used to mark a user as partially deployed."""

    CALLBACK_NAME = 'deploy_users'

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        username = result._result['invocation']['module_args']['name']
        uid = int(result._result['invocation']['module_args']['uid'])
        user = FarmUser.objects.get(username=username)
        # Mark user as partially deployed
        user.deployment_started = True
        user.save()
        # Call the default callback (to display what's going on)
        super(UserDeployCallback, self).v2_runner_on_ok(result, **kwargs)

def deploy_users(userlist=None, machinelist=None, test_mode=False):
    """Create Unix accounts for all Django users on all registered machines.
    If a non-empty list of username is provided, only these user accounts are
    created.

    In any case, no user account is ever deleted.
    """
    # Users to deploy
    users = FarmUser.all_deployable()
    if not (userlist == None or userlist == []):
        users = users.filter(username__in=userlist)
    users = exclude_forbidden_users(users)
    print("== Deploying users: {}".format(', '.join([u.username for u in users])))
    # Machines to deploy on
    if machinelist == None or machinelist == []:
        machines = Machine.objects.all()
    else:
        machines = Machine.objects.filter(name__in=machinelist).all()
        if len(machinelist) != machines.count():
            print("*Warning:* some requested machines could not be found", file=sys.stderr)
    print("== Deploying on machines: {}".format(', '.join([m.name for m in machines])))
    # Hack to handle machines with a different UID scheme (offset).  We
    # create groups of machines with the same offset and run a Ansible
    # play on each group independently.
    for offset in machines.order_by('uid_offset').values_list('uid_offset', flat=True).distinct():
        hosts = list(machines.filter(uid_offset=offset).values_list('hostname', flat=True))
        print("\n== Deploying with UID offset {} on hosts: {}".format(offset, ', '.join(hosts)))
        play = dict(
            name = "Deploy gccfarm users",
            hosts = 'all',
            gather_facts = 'no',
            tasks = [
                dict(action=dict(module='user', name=u.username, uid=u.uid+offset, createhome='yes'),
                     name='User {} ({})'.format(u.username, u.uid))
                for u in users
            ]
        )
        if test_mode:
            run_play(hosts, play, check=test_mode)
        else:
            run_play(hosts, play, check=test_mode, callback_class=UserDeployCallback)


class LatestUserDeployCallback(ansible.plugins.callback.default.CallbackModule):
    """This class is used to track which user was successfully deployed on
    which machine, to update the "largest_deployed_uid" field."""

    CALLBACK_NAME = 'deploy_latest_users'
    # True if at some point a user fails to deploy on a given machine.
    deploy_error = defaultdict(lambda: False)

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        username = result._result['invocation']['module_args']['name']
        uid = int(result._result['invocation']['module_args']['uid'])
        user = FarmUser.objects.get(username=username)
        machine = Machine.objects.get(hostname=host.name)
        # Correct UID to account for UID offset of the machine
        uid -= machine.uid_offset
        # Mark user as partially deployed
        user.deployment_started = True
        user.save()
        # Only update largest_deployed_uid if there has been no error so far
        # (this assumes that users are deployed with increasing UID!)
        if not self.deploy_error[host.name]:
            machine.largest_deployed_uid = max(machine.largest_deployed_uid, uid)
            machine.save()
        # Call the default callback (to display what's going on)
        super(LatestUserDeployCallback, self).v2_runner_on_ok(result, **kwargs)

    def v2_runner_on_failed(self, result, **kwargs):
        self.deploy_error[result._host.name] = True
        super(LatestUserDeployCallback, self).v2_runner_on_failed(result, **kwargs)

    def v2_runner_on_unreachable(self, result, **kwargs):
        self.deploy_error[result._host.name] = True
        super(LatestUserDeployCallback, self).v2_runner_on_unreachable(result, **kwargs)


def deploy_latest_users(test_mode=False):
    """Create missing Unix accounts based on the "largest_deployed_uid" field
    of machines.
    """
    all_users = FarmUser.all_deployable().order_by('uid')
    all_users = exclude_forbidden_users(all_users)
    all_machines = Machine.objects.all()
    # Group machines with the same set of users to deploy
    for largest_deployed_uid in all_machines.order_by('largest_deployed_uid').values_list('largest_deployed_uid', flat=True).distinct():
        users = all_users.filter(uid__gt=largest_deployed_uid)
        if len(users) == 0:
            continue
        machines = all_machines.filter(largest_deployed_uid=largest_deployed_uid)
        print("== Deploying on machines: {}".format(', '.join([m.name for m in machines])))
        print("== Deploying users: {}".format(', '.join([u.username for u in users])))
        for offset in machines.order_by('uid_offset').values_list('uid_offset', flat=True).distinct():
            hosts = list(machines.filter(uid_offset=offset).values_list('hostname', flat=True))
            play = dict(
                name = "Deploy user with UID > {} (UID offset {})".format(largest_deployed_uid, offset),
                hosts = 'all',
                gather_facts = 'no',
                tasks = [
                    dict(action=dict(module='user', name=u.username, uid=u.uid+offset, createhome='yes'),
                         name='User {} ({})'.format(u.username, u.uid))
                    for u in users
                ]
            )
            # Only use the custom callback class when running for real
            if test_mode:
                run_play(hosts, play, check=test_mode)
            else:
                run_play(hosts, play, check=test_mode, callback_class=LatestUserDeployCallback)


def deploy_sshkeys(userlist=None, machinelist=None, test_mode=False):
    """Deploy SSH keys for all Django users on all registered machines.  If a
    non-empty list of username is provided, only SSH keys for these users are
    deployed.

    Previous SSH keys present in ~/.ssh/authorized_keys are deleted.
    """
    users = FarmUser.all_deployable().prefetch_related('userkey_set')
    if not (userlist == None or userlist == []):
        users = users.filter(username__in=userlist)
    users = exclude_forbidden_users(users)
    print("== Deploying for users: {}".format(', '.join([u.username for u in users])))
    # Machines to deploy on
    if machinelist == None or machinelist == []:
        machines = Machine.objects.all()
    else:
        machines = Machine.objects.filter(name__in=machinelist)
        if len(machinelist) != machines.count():
            print("*Warning:* some requested machines could not be found", file=sys.stderr)
    print("== Deploying on machines: {}".format(', '.join([m.name for m in machines])))
    hosts = [m.hostname for m in machines]
    play =  dict(
        name = "Deploy user SSH keys on gccfarm",
        hosts = 'all',
        gather_facts = 'no',
        tasks = [
            dict(action=dict(module='authorized_key', user=u.username, key=u.get_sshkeys(), manage_dir='yes', exclusive='yes'),
                 ignore_errors='yes', name='SSH keys {}'.format(u.username))
            for u in users
        ]
    )
    run_play(hosts, play, check=test_mode)


class SSHKeyDeployQueueCallback(ansible.plugins.callback.default.CallbackModule):
    """This class is used to track which SSH keys were successfully deployed
    on which machine, for users in the main queue.
    """

    CALLBACK_NAME = 'SSHKeyDeployQueue'
    # For each user, record the set of machines on which SSH keys were
    # successfully deployed.
    deploy_success = defaultdict(set)

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        username = result._result['invocation']['module_args']['user']
        self.deploy_success[username].add(host.name)
        # Call the default callback (to display what's going on)
        super(SSHKeyDeployQueueCallback, self).v2_runner_on_ok(result, **kwargs)

def deploy_sshkeys_from_queue(test_mode=False):
    """Deploy SSH keys for Django users listed in the queue of changed SSH
    keys (SSHKeyDeployQueue)

    Previous SSH keys present in ~/.ssh/authorized_keys are deleted.

    Upon success, this function removes users from the queue, and may add
    them to the backlog if deployment failed on some machines (for
    instance if the machine was unreachable, or if it was not possible to
    deploy keys for this user).
    """
    # How many successful machines (as a fraction) are necessary to move a
    # user out of the queue.
    THRESHOLD = 0.45
    machines = list(Machine.objects.all())
    queue = list(SSHKeyDeployQueue.objects.all().select_related('user').prefetch_related('user__userkey_set'))
    queued_users = set(x.user for x in queue if x.user.is_allowed() and x.user.deployment_started)
    # Kept around, to be able to remove items from the queue when deployment of a user is successful
    queue_id = {user.username: [x.pk for x in queue if x.user == user] for user in queued_users}
    hosts = set(m.hostname for m in machines)
    print("== Deploying on hosts: {}".format(', '.join(hosts)))
    print("== Deploying for users: {}".format(', '.join([u.username for u in queued_users])))
    play = dict(
        name = "Deploy user SSH keys on gccfarm",
        hosts = 'all',
        gather_facts = 'no',
        tasks = [
            dict(action=dict(module='authorized_key', user=u.username, key=u.get_sshkeys(), manage_dir='yes', exclusive='yes'),
                 ignore_errors='yes', name='SSH keys {}'.format(u.username))
            for u in queued_users
        ]
    )
    run_play(list(hosts), play, check=test_mode, callback_class=SSHKeyDeployQueueCallback)
    for (username, deployed_machines) in SSHKeyDeployQueueCallback.deploy_success.items():
        # Success
        if len(deployed_machines) >= THRESHOLD * len(hosts):
            remaining_machines = hosts - deployed_machines
            user = FarmUser.objects.get(username=username)
            # Add user to backlog for failed machines
            for hostname in remaining_machines:
                machine = Machine.objects.get(hostname=hostname)
                print("= Adding ({}, {}) to backlog".format(username, hostname))
                if not test_mode:
                    SSHKeyDeployBacklog.objects.get_or_create(user=user, machine=machine)
            # Remove user from queue
            print("= Removing user {} from queue".format(username))
            if not test_mode:
                SSHKeyDeployQueue.objects.filter(pk__in=queue_id[username]).delete()


class SSHKeyDeployBacklogCallback(ansible.plugins.callback.default.CallbackModule):
    """This class is used to track which SSH keys were successfully deployed
    on which machine, for users in the backlog queue.
    """

    CALLBACK_NAME = 'SSHKeyDeployBacklog'
    test_mode = False
    unreachable_machines = set()

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        username = result._result['invocation']['module_args']['user']
        if not self.test_mode:
            # Remove from backlog
            user = FarmUser.objects.get(username=username)
            machine = Machine.objects.get(hostname=host.name)
            SSHKeyDeployBacklog.objects.get(user=user, machine=machine).delete()
        # Call the default callback (to display what's going on)
        super(SSHKeyDeployBacklogCallback, self).v2_runner_on_ok(result, **kwargs)

    def v2_runner_on_unreachable(self, result, **kwargs):
        host = result._host
        self.unreachable_machines.add(host.name)
        # Call the default callback (to display what's going on)
        super(SSHKeyDeployBacklogCallback, self).v2_runner_on_unreachable(result, **kwargs)

def deploy_sshkeys_from_backlog(test_mode=False):
    """Deploy SSH keys for users and machines from the backlog (SSHKeyDeployBacklog)

    Previous SSH keys present in ~/.ssh/authorized_keys are deleted.

    Upon success, this function removes (user, machine) couples from the backlog.
    """
    SSHKeyDeployBacklogCallback.test_mode = test_mode
    for backlog in SSHKeyDeployBacklog.objects.all().select_related('user', 'machine').prefetch_related('user__userkey_set'):
        user = backlog.user
        machine = backlog.machine
        # Remove forbidden user
        if user.is_forbidden():
            print("** WARNING: skipping forbidden user '{}' **".format(user.username))
            continue
        # Optimisation: when a machine is unreachable, stop trying to deploy on it
        if machine.hostname in SSHKeyDeployBacklogCallback.unreachable_machines:
            print("== Skipping user '{}' on machine '{}' because machine is unreachable".format(user.username, machine.name))
            continue
        sshkeys = user.get_sshkeys()
        play = dict(
            name = "Deploy SSH keys for user '{}' on machine '{}'".format(user.username, machine.name),
            hosts = 'all',
            gather_facts = 'no',
            tasks = [
                dict(action=dict(module='authorized_key', user=user.username, key=sshkeys, manage_dir='yes', exclusive='yes'),
                 ignore_errors='yes', name='SSH keys {}'.format(user.username))
            ]
        )
        run_play([machine.hostname], play, check=test_mode, callback_class=SSHKeyDeployBacklogCallback)
