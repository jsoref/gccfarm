from collections import namedtuple

from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback import CallbackBase


def run_play(hosts_list, play_source, callback_class=None, remote_user='root', check=False):
    Options = namedtuple('Options', ['remote_user', 'connection', 'module_path', 'forks', 'become', 'become_method', 'become_user', 'check', 'diff'])

    # initialize needed objects
    variable_manager = VariableManager()
    loader = DataLoader()
    options = Options(remote_user=remote_user, connection='ssh', module_path=None, forks=50, become=None, become_method=None, become_user=None, check=check, diff=check)

    # Instantiate callback class for handling results as they come in
    if callback_class != None:
        callback = callback_class()
    else:
        callback = None

    # create inventory and pass to var manager
    inventory = Inventory(loader=loader, variable_manager=variable_manager, host_list=hosts_list)
    variable_manager.set_inventory(inventory)

    play = Play().load(play_source, variable_manager=variable_manager, loader=loader)

    # actually run it
    tqm = None
    try:
        tqm = TaskQueueManager(
            inventory=inventory,
            variable_manager=variable_manager,
            loader=loader,
            options=options,
            passwords=None,
            stdout_callback=callback,
          )
        result = tqm.run(play)
    finally:
        if tqm is not None:
            tqm.cleanup()
