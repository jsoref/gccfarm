# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

""" Use ansible to gather information about managed machines: CPU type,
memory, OS, etc...

"""

import sys
import json
import datetime

import django.utils.timezone
from ansible.plugins.callback import CallbackBase

from gccfarm_machine.models import Machine
from boilerplate import run_play


class ResultCallback(CallbackBase):
    QUIET = False

    def print(self, msg):
        if not self.QUIET:
            print(msg)

    def print_stderr(self, msg):
        if not self.QUIET:
            print(msg, file=sys.stderr)

    def parse_facts(self, facts):
        """Parse facts provided by ansible.  Returns a dictionary mapping variable
        name to variable value.  Each value is guaranteed to be either a string
        or an integer.
        """
        res = dict()
        for fact in (
                'ansible_architecture', 'ansible_machine', # Often identical (but sometimes not)
                'ansible_system', # Linux, AIX, NetBSD...
                'ansible_system_vendor', # Dell... (sometimes not relevant at all)
                'ansible_product_name',
                # os_family is generally too broad (e.g. 'Debian' for Ubuntu)
                'ansible_os_family', 'ansible_distribution', 'ansible_distribution_release', 'ansible_distribution_version', 'ansible_distribution_major_version',
                #'ansible_lsb', # Often not present
                'ansible_kernel',
                'ansible_userspace_bits',
                'ansible_memtotal_mb', 'ansible_swaptotal_mb',
                # processor_count and processor_vcpus are sometimes 0 (EdgeRouter)
                'ansible_processor_count', 'ansible_processor_cores', 'ansible_processor_threads_per_core', 'ansible_processor_vcpus',
                'ansible_processor', # Often a list, sometimes a string
                'ansible_mounts', # Dictionary
                'ansible_uptime_seconds',
        ):
            try:
                value = facts[fact]
            except KeyError:
                continue
            # Parse a few special cases
            if fact == 'ansible_mounts':
                for mountpoint in value:
                    if mountpoint['mount'] == '/home':
                        res['mount_home_size_total'] = mountpoint['size_total']
                        res['mount_home_size_available'] = mountpoint['size_available']
                        res['mount_home_fstype'] = mountpoint['fstype']
                    if mountpoint['mount'] == '/':
                        res['mount_root_size_total'] = mountpoint['size_total']
                        res['mount_root_size_available'] = mountpoint['size_available']
                        res['mount_root_fstype'] = mountpoint['fstype']
            elif fact == 'ansible_processor':
                if not isinstance(value, str if sys.version_info[0] >= 3 else basestring):
                    value = set(value)
                    if len(value) > 1:
                        # Remove useless information
                        value = { proc for proc in value if "AuthenticAMD" not in proc and "GenuineIntel" not in proc }
                    # Just concatenate remaining items, in the unlikely
                    # case that there is still more than one
                    value = ' / '.join(value)
                res['processor'] = value
            # By default, copy Ansible variable (useful for debugging)
            res[fact] = value

        # Handle OS
        version = res.get('ansible_distribution_version', '')
        # NetBSD has a super-long version string
        if len(version) > 20:
            version = ''
        res['os'] = '{} {} {}'.format(res.get('ansible_distribution', ''),
                                        version,
                                        res.get('ansible_distribution_release', ''))

        # Handle CPU
        if not isinstance(res.get('ansible_processor_cores', 1), int):
            res['ansible_processor_cores'] = 1
        res['nb_cpu'] = res.get('ansible_processor_count', 1)
        res['nb_cores'] = res.get('ansible_processor_cores', 1) * res['nb_cpu']
        res['nb_threads'] = res.get('ansible_processor_threads_per_core', 1) * res['nb_cores']

        # Handle uptime
        res['uptime'] = datetime.timedelta(seconds=res.get('ansible_uptime_seconds', 0))
        return res

    def update_database(self, hostname, variables):
        try:
            m = Machine.objects.get(hostname=hostname)
        except Machine.DoesNotExist:
            self.print_stderr('Error fetching machine {} from database'.format(hostname))
            return

        def update_field(field, var_name):
            if var_name in variables:
                setattr(m, field, variables[var_name])

        update_field('architecture', 'ansible_architecture')
        update_field('kernel', 'ansible_kernel')
        update_field('os', 'os')
        update_field('processor', 'processor')
        update_field('nb_cpu', 'nb_cpu')
        update_field('nb_cores', 'nb_cores')
        update_field('nb_threads', 'nb_threads')
        update_field('memtotal_mb', 'ansible_memtotal_mb')
        update_field('swaptotal_mb', 'ansible_swaptotal_mb')
        update_field('mount_root_fstype', 'mount_root_fstype')
        update_field('mount_root_size_total', 'mount_root_size_total')
        update_field('mount_root_size_available', 'mount_root_size_available')
        update_field('mount_home_fstype', 'mount_home_fstype')
        update_field('mount_home_size_total', 'mount_home_size_total')
        update_field('mount_home_size_available', 'mount_home_size_available')
        update_field('uptime', 'uptime')
        m.success = True
        m.last_success = django.utils.timezone.now()
        try:
            m.save()
        except AttributeError, ValueError:
            self.print_stderr('Error saving details for machine {}'.format(hostname))

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        # Use this to display all available raw facts
        #print(json.dumps({host.name: result._result}, indent=4))
        variables = self.parse_facts(result._result['ansible_facts'])
        # Simply display all known variables for now
        self.print("\n== {} ==".format(host.name))
        for (key, value) in variables.items():
            self.print('{}: {}'.format(key, value))
        self.update_database(host.name, variables)

    def v2_runner_on_failed(self, result, **kwargs):
        host = result._host
        m = Machine.objects.get(hostname=host.name)
        m.success = False
        m.save()
        self.print("\n==== Failed {}".format(host))
        self.print(json.dumps({host.name: result._result}, indent=4))

    def v2_runner_on_unreachable(self, result, **kwargs):
        host = result._host
        m = Machine.objects.get(hostname=host.name)
        m.success = False
        m.save()
        self.print("\n==== Unreachable {}".format(host))
        self.print(json.dumps({host.name: result._result}, indent=4))


def gather_info(machines=None, quiet=False):
    ResultCallback.QUIET = quiet
    if machines == None or machines == []:
        hosts = [m.hostname for m in Machine.objects.all()]
    else:
        hosts = [m.hostname for m in Machine.objects.filter(name__in=machines)]
    play = dict(
        name = "Gather information",
        hosts = 'all',
        gather_facts = 'yes',
        tasks = []
    )
    run_play(hosts, play, callback_class=ResultCallback)
